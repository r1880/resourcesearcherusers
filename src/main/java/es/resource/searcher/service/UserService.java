package es.resource.searcher.service;

import java.util.List;

import es.resource.searcher.entities.User;

public interface UserService {

	User validateUser(User user);
	
	void insertUser(User user);
	
	void deleteUser(String username);
	
	void updateUser(User user);

	List<User> getUsers();

	User getUser(String userEmail);
	
}
