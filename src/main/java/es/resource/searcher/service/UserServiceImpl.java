package es.resource.searcher.service;

import java.util.List;

import es.resource.searcher.constants.UserConstans;
import es.resource.searcher.entities.User;
import es.valhalla.business.component.AbstractBusinessComponent;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.utils.hash.HashUtils;
import jakarta.annotation.PostConstruct;

public class UserServiceImpl extends AbstractBusinessComponent implements UserService {

	private String url;
	private String databaseName;
	private String user;
	private String secret;
	private String collection;

	@PostConstruct
	private void init() {
		this.url = getStringProperty(UserConstans.APP_NAME, ValahallaConstants.MONGO_URL);
		this.databaseName = getStringProperty(UserConstans.APP_NAME, ValahallaConstants.MONGO_DBNAME);
		this.user = getStringProperty(UserConstans.APP_NAME, ValahallaConstants.MONGO_USER);
		this.secret = getStringProperty(UserConstans.APP_NAME, ValahallaConstants.MONGO_SECRET);
		this.collection = getStringProperty(UserConstans.APP_NAME, UserConstans.COLLECTION_NAME);
	}

	@Override
	public User validateUser(final User userRh) {
		User retrieved = getMongo(url, user, secret, databaseName, collection, userRh.getId(), User.class);
		if (retrieved != null) {
			String hashed = new String(HashUtils.hashPBKDF2(userRh.getPwd(), retrieved.getSalt()));
			retrieved = retrieved.getPwd().equals(hashed) ? retrieved : null;
		}
		return retrieved;

	}

	@Override
	public void insertUser(final User userRh) {
		userRh.setId(userRh.getUsername());
		userRh.setSalt(generateSalt());
		userRh.setPwd(hashPBKDF2ToString(userRh.getPwd(), userRh.getSalt()));
		insertMongo(url, user, secret, databaseName, collection, userRh, User.class);
	}

	@Override
	public void deleteUser(final String username) {
		deleteMongo(url, user, secret, databaseName, collection, username, User.class);
	}


	@Override
	public void updateUser(final User userRh) {
		final String id = userRh.getId() !=null ? userRh.getId() : userRh.getUsername(); 
		final User userRetrieved = getMongo(url, user, secret, databaseName, collection, id, User.class);
		if (userRetrieved != null) {
			userRetrieved.setName(userRh.getName());
			userRetrieved.setSurname(userRh.getSurname());
			userRetrieved.setRole(userRh.getRole());
			if (userRh.getPwd() != null) {
				userRetrieved.setSalt(HashUtils.generateSalt());
				userRetrieved.setPwd(new String(HashUtils.hashPBKDF2(userRh.getPwd(), userRh.getSalt())));

			}
		}

		updateMongo(url, user, secret, databaseName, collection, userRetrieved, User.class);
	}

	@Override
	public User getUser(final String userEmail) {
		return sanitize(getMongo(url, user, secret, databaseName, collection, userEmail, User.class));
	}

	@Override
	public List<User> getUsers() {

		List<User> users = getAllMongo(url, user, secret, databaseName, collection, User.class);
		users.forEach(u -> {
			u = sanitize(u);
		});
		return users;

	}

	private User sanitize(User user) {
		user.setPwd(null);
		user.setSalt(null);
		return user;
	}

}
