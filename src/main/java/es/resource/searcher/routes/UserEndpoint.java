package es.resource.searcher.routes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import es.resource.searcher.constants.UserConstans;
import es.resource.searcher.entities.User;
import es.resource.searcher.service.UserService;
import es.valhalla.business.component.AbstractBaseHttpHandler;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.entities.ValhallaServiceRegistry;
import es.valhalla.exception.ValhallaBusinessExceptionEnum;
import es.valhalla.jwt.entities.JWTRoleENUM;
import es.valhalla.jwt.service.JWTService;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class UserEndpoint extends AbstractBaseHttpHandler {

	@Inject
	UserService userService;

	@Inject
	JWTService jwtService;

	private Map<String, List<ValhallaServiceRegistry>> serviceRegistry;

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(UserEndpoint.class);

	private static enum ENDPOINTS {
		INSERT, UPDATE, USER, USERS, DELETE
	};

	public void insertUser(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.POST.name(),
				UserConstans.SERVICE_PORT, UserConstans.SERVICE_PATH, getSelfAddress());
		serviceRegistry.get(UserConstans.APP_NAME).add(self);
		buildRoute(router, UserConstans.SERVICE_PATH, HttpMethod.POST, MediaType.APPLICATION_JSON)
				.handler(ctx -> proccess(ctx, ENDPOINTS.INSERT)).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	public void updateUser(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.PUT.name(),
				UserConstans.SERVICE_PORT, UserConstans.SERVICE_PATH, getSelfAddress());
		serviceRegistry.get(UserConstans.APP_NAME).add(self);
		buildRoute(router, UserConstans.SERVICE_PATH, HttpMethod.PUT, MediaType.APPLICATION_JSON)
				.handler(ctx -> proccess(ctx, ENDPOINTS.UPDATE)).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));
	}

	public void getUser(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.GET.name(),
				UserConstans.SERVICE_PORT, UserConstans.SERVICE_PATH + "/user/:id", getSelfAddress());
		serviceRegistry.get(UserConstans.APP_NAME).add(self);
		buildRouteEmpty(router, UserConstans.SERVICE_PATH + "/user/:id", HttpMethod.GET, MediaType.APPLICATION_JSON)
				.handler(ctx -> proccess(ctx, ENDPOINTS.USER)).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	public void getUsers(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.GET.name(),
				UserConstans.SERVICE_PORT, UserConstans.SERVICE_PATH, getSelfAddress());
		serviceRegistry.get(UserConstans.APP_NAME).add(self);
		buildRouteEmpty(router, UserConstans.SERVICE_PATH, HttpMethod.GET, MediaType.APPLICATION_JSON)
				.handler(ctx -> proccess(ctx, ENDPOINTS.USERS)).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	public void deleteUser(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.DELETE.name(),
				UserConstans.SERVICE_PORT, UserConstans.SERVICE_PATH, getSelfAddress());
		serviceRegistry.get(UserConstans.APP_NAME).add(self);
		buildRouteEmpty(router, UserConstans.SERVICE_PATH, HttpMethod.DELETE, MediaType.APPLICATION_JSON)
				.handler(ctx -> proccess(ctx, ENDPOINTS.DELETE)).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	private void proccess(final RoutingContext ctx, final ENDPOINTS endpoint) {
		final long init = System.currentTimeMillis();

		final String token = getTokenFromHeader(ctx.request());
		if (token != null) {
			final ValhallaServiceRegistry idpService = serviceRegistry.get(ValahallaConstants.AUTH_SERVICE).stream()
					.filter(s -> s.getServicePath().contains("validate")).findFirst().orElse(null);
			if (idpService == null) {
				LOGGER.error("IDP not registered");
				throw ValhallaBusinessExceptionEnum.IDP_NOT_REGISTERED.getException();
			}
			idpValidation(idpService.getServiceAddress(), idpService.getServicePath(), idpService.getPort(), token)
					.onSuccess(handler -> {
						if (handler.statusCode() == HttpResponseStatus.OK.code()) {
							if (jwtService.getClaims(token).getRole().name().equals(JWTRoleENUM.ADMIN.name())) {

								final String serialized = executedOption(ctx, endpoint);
								processSuccessHttpResponse(ctx, serialized);
							} else {
								processUnauthorizedResponse(ctx);
							}
						} else {
							processUnauthorizedResponse(ctx);
						}

					}).onFailure(handler -> {
						processUnauthorizedResponse(ctx);
					}).onComplete(
							finished -> LOGGER.info("Request processed in {} ms status code {}", System.currentTimeMillis() - init,finished.result().statusCode()));
		} else {
			processBadRequestResponse(ctx);
		}
	}

	private String executedOption(final RoutingContext ctx, final ENDPOINTS endpoint) {
		String serialized = null;


		switch (endpoint) {

		case USER:
			serialized = serialize(userService.getUser(ctx.pathParam(UserConstans.ID)));
			break;
		case USERS:
			serialized = serialize(userService.getUsers());
			break;
		case INSERT:
			userService.insertUser(deserialize(ctx.getBodyAsString(), User.class));
			serialized = "";
			break;
		case UPDATE:
			userService.updateUser(deserialize(ctx.getBodyAsString(), User.class));
			serialized = "";
			break;
		case DELETE:
			userService.deleteUser(deserialize(ctx.getBodyAsString(), User.class).getUsername());
			serialized = "";
			break;

		}
		return serialized;
	}

	public void enableEndpoints(final Router router, final Map<String, List<ValhallaServiceRegistry>> serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
		serviceRegistry.put(UserConstans.APP_NAME, new ArrayList<>());
		insertUser(router);
		getUsers(router);
		updateUser(router);
		deleteUser(router);

	}

}
