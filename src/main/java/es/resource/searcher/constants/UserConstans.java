package es.resource.searcher.constants;

public class UserConstans {

	public static final String APP_NAME = "User";
	public static final String SERVICE_PATH = "/resourcesearcher/rest/service/users";
	public static final int SERVICE_PORT = 8084;
	public static final String COLLECTION_NAME = "mongo.collection";
	public static final String ID = "id";
	public static final String SERVICE_REGISTRY_PROP = "service.registry.path";
	/** LOCAL PROPERTY **/
	public static final String DEFAULT_REGISTRY_PATH = "C:\\ResoureSearcher\\resource_searcher.json";
}
